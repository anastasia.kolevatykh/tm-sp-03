package ru.kolevatykh.spring.controller.mvc;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kolevatykh.spring.enumerate.StatusType;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Project;
import ru.kolevatykh.spring.model.User;
import ru.kolevatykh.spring.service.ProjectService;
import ru.kolevatykh.spring.service.UserService;
import ru.kolevatykh.spring.util.DateFormatterUtil;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    private final UserService userService;

    @NotNull
    private final ProjectService projectService;

    @Autowired
    public ProjectController(
            @NotNull final UserService userService,
            @NotNull final ProjectService projectService
    ) {
        this.userService = userService;
        this.projectService = projectService;
    }

    @GetMapping("/project-list")
    public String findAll(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAll();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-create")
    public String createProjectForm(@NotNull final Model model) throws Exception {
        return "project-create";
    }

    @PostMapping("/project-create")
    public String createProject(
            @ModelAttribute("userId") @Nullable final String userId,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("statusType") @Nullable final String statusType,
            @ModelAttribute("startDate") @Nullable final String startDate,
            @ModelAttribute("finishDate") @Nullable final String finishDate
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        if (name == null) throw new EmptyInputException("project name");
        @NotNull final Project project = new Project(name, description, StatusType.valueOf(statusType));
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        project.setUser(user);
        project.setStartDate(DateFormatterUtil.parseDate(startDate));
        project.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        projectService.persist(project);
        return "redirect:/project-list";
    }

    @GetMapping("/project-delete/{userId}/{id}")
    public String deleteProject(
            @PathVariable("userId") @NotNull final String userId,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        projectService.remove(userId, id);
        return "redirect:/project-list";
    }

    @GetMapping("/project-update/{userId}/{id}")
    public String updateProjectForm(
            @PathVariable("userId") @NotNull final String userId,
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        @Nullable final Project project = projectService.findOneById(userId, id);
        model.addAttribute("project", project);
        return "project-update";
    }

    @PostMapping("/project-update")
    public String updateProject(
            @ModelAttribute("userId") @Nullable final String userId,
            @ModelAttribute("id") @Nullable final String id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("statusType") @Nullable final String statusType,
            @ModelAttribute("startDate") @Nullable final String startDate,
            @ModelAttribute("finishDate") @Nullable final String finishDate
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        @Nullable final Project project = projectService.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        project.setUser(user);
        if (name == null) throw new EmptyInputException("project name");
        project.setName(name);
        project.setDescription(description);
        project.setStatusType(StatusType.valueOf(statusType));
        project.setStartDate(DateFormatterUtil.parseDate(startDate));
        project.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        projectService.merge(project);
        return "redirect:/project-list";
    }

    @GetMapping("/project-view/{userId}/{id}")
    public String viewProjectForm(
            @PathVariable("userId") @NotNull final String userId,
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        if (id.isEmpty()) throw new ProjectNotFoundException();
        if (userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final Project project = projectService.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        model.addAttribute("project", project);
        return "project-view";
    }

    @Nullable
    @GetMapping("/project-search")
    public String findAllBySearch(
            @RequestParam("search") @NotNull final String search,
            @NotNull final Model model
    ) throws Exception {
        @NotNull final List<Project> projectSearch = projectService.findAllBySearch(search);
        if (projectSearch.isEmpty()) return null;
        model.addAttribute("projectSearch", projectSearch);
        return "project-search";
    }

    @GetMapping("/project-list-start-date-asc")
    public String findAllByStartDateAsc(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAllSortedByStartDateAsc();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-list-start-date-desc")
    public String findAllByStartDateDesc(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAllSortedByStartDateDesc();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-list-finish-date-asc")
    public String findAllByFinishDateAsc(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAllSortedByFinishDateAsc();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-list-finish-date-desc")
    public String findAllByFinishDateDesc(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAllSortedByFinishDateDesc();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-list-status-asc")
    public String findAllByStatusAsc(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAllSortedByStatusAsc();
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/project-list-status-desc")
    public String findAllByStatusDesc(@NotNull final Model model) throws Exception {
        @NotNull final List<Project> projects = projectService.findAllSortedByStatusDesc();
        model.addAttribute("projects", projects);
        return "project-list";
    }
}