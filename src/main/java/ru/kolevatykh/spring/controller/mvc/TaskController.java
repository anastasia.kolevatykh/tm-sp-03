package ru.kolevatykh.spring.controller.mvc;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kolevatykh.spring.enumerate.StatusType;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.TaskNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Task;
import ru.kolevatykh.spring.model.User;
import ru.kolevatykh.spring.service.ProjectService;
import ru.kolevatykh.spring.service.TaskService;
import ru.kolevatykh.spring.service.UserService;
import ru.kolevatykh.spring.util.DateFormatterUtil;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    private final UserService userService;

    @NotNull
    private final ProjectService projectService;

    @NotNull
    private final TaskService taskService;

    @Autowired
    public TaskController(
            @NotNull final UserService userService,
            @NotNull final ProjectService projectService,
            @NotNull final TaskService taskService
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @GetMapping("/task-list")
    public String findAll(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-create")
    public String createTask(@NotNull final Model model) throws Exception {
        return "task-create";
    }

    @PostMapping("/task-create")
    public String createTask(
            @ModelAttribute("userId") @Nullable final String userId,
            @ModelAttribute("projectId") @Nullable final String projectId,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("statusType") @NotNull final String statusType,
            @ModelAttribute("startDate") @Nullable final String startDate,
            @ModelAttribute("finishDate") @Nullable final String finishDate
    ) throws Exception, TaskNotFoundException {
        if (name == null) throw new EmptyInputException("task name");
        @NotNull final Task task = new Task(name, description, StatusType.valueOf(statusType));
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        task.setUser(user);
        task.setProject(projectService.findOneById(userId, projectId));
        task.setStartDate(DateFormatterUtil.parseDate(startDate));
        task.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        taskService.persist(task);
        return "redirect:/task-list";
    }

    @GetMapping("/task-delete/{userId}/{id}")
    public String deleteTask(
            @PathVariable("userId") @NotNull final String userId,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        taskService.remove(userId, id);
        return "redirect:/task-list";
    }

    @GetMapping("/task-update/{userId}/{id}")
    public String updateTaskForm(
            @PathVariable("userId") @NotNull final String userId,
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        @Nullable final Task task = taskService.findOneById(userId, id);
        model.addAttribute("task", task);
        return "task-update";
    }

    @PostMapping("/task-update")
    public String updateTask(
            @ModelAttribute("userId") @Nullable final String userId,
            @ModelAttribute("projectId") @Nullable final String projectId,
            @ModelAttribute("id") @Nullable final String id,
            @ModelAttribute("name") @Nullable final String name,
            @ModelAttribute("description") @Nullable final String description,
            @ModelAttribute("statusType") @NotNull final String statusType,
            @ModelAttribute("startDate") @Nullable final String startDate,
            @ModelAttribute("finishDate") @Nullable final String finishDate
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        @Nullable final Task task = taskService.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        task.setProject(projectService.findOneById(userId, projectId));
        if (name == null) throw new EmptyInputException("task name");
        task.setName(name);
        task.setDescription(description);
        task.setStatusType(StatusType.valueOf(statusType));
        task.setStartDate(DateFormatterUtil.parseDate(startDate));
        task.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        taskService.merge(task);
        return "redirect:/task-list";
    }

    @GetMapping("/task-view/{userId}/{id}")
    public String viewProjectForm(
            @PathVariable("userId") @NotNull final String userId,
            @PathVariable("id") @NotNull final String id,
            @NotNull final Model model
    ) throws Exception {
        if (id.isEmpty()) throw new TaskNotFoundException();
        if (userId.isEmpty()) throw new UserNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        model.addAttribute("task", task);
        return "task-view";
    }

    @Nullable
    @GetMapping("/task-search")
    public String findAllBySearch(
            @RequestParam("search") @NotNull final String search,
            @NotNull final Model model
    ) throws Exception {
        @NotNull final List<Task> taskSearch = taskService.findAllBySearch(search);
        if (taskSearch.isEmpty()) return null;
        model.addAttribute("taskSearch", taskSearch);
        return "task-search";
    }

    @GetMapping("/task-list-start-date-asc")
    public String findAllByStartDateAsc(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAllSortedByStartDateAsc();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-list-start-date-desc")
    public String findAllByStartDateDesc(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAllSortedByStartDateDesc();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-list-finish-date-asc")
    public String findAllByFinishDateAsc(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAllSortedByFinishDateAsc();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-list-finish-date-desc")
    public String findAllByFinishDateDesc(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAllSortedByFinishDateDesc();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-list-status-asc")
    public String findAllByStatusAsc(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAllSortedByStatusAsc();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-list-status-desc")
    public String findAllByStatusDesc(@NotNull final Model model) throws Exception {
        @NotNull final List<Task> tasks = taskService.findAllSortedByStatusDesc();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }
}
